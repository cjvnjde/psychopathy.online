import localizedStrings from './localizedStrings';

export const TYPES = [
  { index: 0, value: localizedStrings.empty },
  { index: 1, value: localizedStrings.radio },
  { index: 2, value: localizedStrings.range },
];

export const t = 0;
