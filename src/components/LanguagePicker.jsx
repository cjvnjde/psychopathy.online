import React, { Component } from 'react';
import { LiHeader2 } from '../styled/Header';
import localizedStrings from '../localizedStrings';

class LanguagePicker extends Component {
  handleClick = (e) => {
    localizedStrings.setLanguage(e.target.innerText);
    localStorage.setItem('language', e.target.innerText);
  }

  getBody = () => localizedStrings
    .getAvailableLanguages()
    .map(value => <LiHeader2 key={value} onClick={this.handleClick}>{value}</LiHeader2>)

  render() {
    return (
      <React.Fragment>
        {this.getBody()}
      </React.Fragment>
    );
  }
}

export default LanguagePicker;
