import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';


export default class Main extends Component {
  render() {
    const instruction = '[Работа с формулами](https://gitlab.com/cjvnjde/psychopathy.online/blob/master/formulas.md)\n\n[Работа с сайтом](https://gitlab.com/cjvnjde/psychopathy.online/blob/master/siteworking.md)';
    return (
      <ReactMarkdown source={instruction} />
    );
  }
}
