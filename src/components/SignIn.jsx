import Auth from '../Auth/Auth';

const auth = new Auth();


function SignIn() {
  return auth.login();
}

export default SignIn;
