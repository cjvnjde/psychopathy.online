import React from 'react';
import { Query } from 'react-apollo';
import { PropTypes } from 'prop-types';

import QuestionsList from '../Questions/QuestionsList';
import { GET_QUESTIONNAIRE_ID, GET_QUESTIONNAIRE } from '../../../gql/questionnaire';
import Loading from '../../common/Loading';
import questionnaireStore from '../../../stores/questionnaire';
import SendAnswers from './SendAnswers';
import { dataParser } from '../../../helpers';
import { RespondentContainer } from '../../../styled/RespondentStyled';
import { AuthContext } from '../../../routes';
import NeedAuth from '../../common/NeedAuth';

const Respondent = (props) => {
  const { match } = props;

  const link = match.params.id.substring(1, match.params.id.length);
  questionnaireStore.questionnaireId.set(link);

  return (
    <RespondentContainer>
      <AuthContext.Consumer>
        {auth => (auth.isAuthenticated() ? null : <NeedAuth />)}
      </AuthContext.Consumer>
      <QuestionsList />
      <SendAnswers />
      <Query
        query={GET_QUESTIONNAIRE_ID}
        variables={{ link }}
      >
        {(respQId) => {
          const { loading, error, data } = respQId;
          if (loading) return <Loading />;
          if (error) return <div>Error :(</div>;
          if (data.shared_links.length > 0) {
            const questionsId = data.shared_links[0].questions_id;
            return (
              <Query
                query={GET_QUESTIONNAIRE}
                variables={{ id: questionsId }}
              >
                {(respQ) => {
                  const loadingQ = respQ.loading;
                  const errorQ = respQ.error;
                  const dataQ = respQ.data;

                  if (loadingQ) return <Loading />;
                  if (errorQ) return <div>Error :(</div>;
                  dataQ && dataParser(dataQ.questionnaire[0], questionnaireStore);
                  return null;
                }}
              </Query>
            );
          }
          return null;
        }}
      </Query>
    </RespondentContainer>
  );
};

Respondent.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};

export default Respondent;
