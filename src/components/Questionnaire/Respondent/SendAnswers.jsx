import React, { Component } from 'react';
import { toJS } from 'mobx';
import { Mutation } from 'react-apollo';

import { ButtonSend } from '../../../styled/RespondentStyled';
import questionnaireStore from '../../../stores/questionnaire';
import userStore from '../../../stores/userStore';
import localizedStrings from '../../../localizedStrings';
import { SET_ANSAWERS } from '../../../gql/questionnaire';

class SendAnswers extends Component {
  render() {
    return (

      <Mutation mutation={SET_ANSAWERS}>

        {insertAnswers => (
          <ButtonSend

            type="button"
            onClick={(e) => {
              let isOk = true;
              questionnaireStore.selectedValues.forEach((value) => {
                if (isOk && value === -1) {
                  isOk = false;
                  alert('Not all answers selected');
                }
              });
              if (isOk) {
                insertAnswers({
                  variables: {
                    objects: [
                      {
                        answers: JSON.stringify(toJS(questionnaireStore.selectedValues)),
                        questions_id: questionnaireStore.questionnaireId.get(),
                        respondent_id: userStore.sub.get(),
                        respondent_nickname: userStore.nickname.get(),
                      },
                    ],
                  },
                });
                alert('sent');
              }
            }
      }
          >
            {localizedStrings.sendAns}
          </ButtonSend>
        )}

      </Mutation>
    );
  }
}

export default SendAnswers;
