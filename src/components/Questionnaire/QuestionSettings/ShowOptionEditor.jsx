import React from 'react';
import PropTypes from 'prop-types';
// helpers
import localizedStrings from '../../../localizedStrings';
// styled
import { LabelShowEditEdit } from '../../../styled/QuestionSettingsStyled';

const ShowOptionEditor = (props) => {
  const { selected, handleChange } = props;

  return (
    <LabelShowEditEdit htmlFor="option_editor">
      <input
        id="option_editor"
        type="checkbox"
        checked={selected}
        onChange={handleChange}
      />
      {localizedStrings.showOptionEdit}
    </LabelShowEditEdit>
  );
};

ShowOptionEditor.propTypes = {
  selected: PropTypes.bool.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default ShowOptionEditor;
