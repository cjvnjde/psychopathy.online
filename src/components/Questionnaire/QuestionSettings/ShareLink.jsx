import React from 'react';
import { Mutation } from 'react-apollo';
import { observer } from 'mobx-react';
import SHA256 from 'crypto-js/sha256';
import questionnaireStore from '../../../stores/questionnaire';
import userStore from '../../../stores/userStore';
import localizedStrings from '../../../localizedStrings';
import { ButtonS } from '../../../styled/SettingsStyled';
import { SET_SHARED_LINK } from '../../../gql/questionnaire';

const ShareLink = (props) => {
  const questionnaireId = questionnaireStore.questionnaireId.get();

  if (userStore.sub.get() && questionnaireId) {
    const link = `${questionnaireId}boobs${SHA256(userStore.sub)}`;
    // console.log(`${window.location.href}:${link}`);
    let location = window.location.href;
    location = `${location.substring(0, location.lastIndexOf('/'))}/respondent:${link}`;
    return (
      <Mutation mutation={SET_SHARED_LINK}>
        {insertSharedLinks => (
          <ButtonS
            type="button"
            onClick={(e) => {
              console.log(location);
              alert(location);
              insertSharedLinks({
                variables: {
                  objects: [
                    {
                      link,
                      questions_id: questionnaireId,
                      user_id: userStore.sub,
                    },
                  ],
                },
              });
            }}
          >
            {localizedStrings.share}
          </ButtonS>
        )}
      </Mutation>
    );
  }
  return null;
};

export default observer(ShareLink);
