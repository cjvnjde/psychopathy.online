import React from 'react';
import PropTypes from 'prop-types';
// helpers
import { TYPES } from '../../../constants';
// styled
import { SelectOption } from '../../../styled/QuestionSettingsStyled';

const TypeSelection = (props) => {
  const { selected, handleChange } = props;

  const options = TYPES.map(val => (
    <option key={val.index} value={val.index}>
      {val.value}
    </option>
  ));

  return (
    <SelectOption value={selected} onChange={handleChange}>
      {options}
    </SelectOption>
  );
};

TypeSelection.propTypes = {
  selected: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default TypeSelection;
