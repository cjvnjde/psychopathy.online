import React from 'react';
// store
import questionnaireStore from '../../../stores/questionnaire';
// styled
import { InputLoad, LabelLoad } from '../../../styled/QuestionSettingsStyled';
// helpers
import localizedStrings from '../../../localizedStrings';

export default () => {
  const fileLoader = (e) => {
    const file = e.target.files;
    const reader = new FileReader();
    questionnaireStore.questions.clear();
    reader.onload = () => {
      const text = reader.result;
      const parsedText = dataParser(text);
      parsedText.forEach((item, i) => {
        questionnaireStore.questions.push({
          index: i, question: item,
        });
        questionnaireStore.selectedValues.set(+i, -1);
      });
    };
    if (file.length === 1) { reader.readAsText(file[0]); }
  };

  const dataParser = (text) => {
    let arr = text.split(/\r?\n/);
    arr = arr.filter(element => element.length > 3);
    arr = arr.map(element => element.trim());
    return arr;
  };

  return (
    <LabelLoad htmlFor="question-loader">
      <InputLoad
        type="file"
        id="question-loader"
        onChange={fileLoader}
      />
      {localizedStrings.loadQuestions}
    </LabelLoad>
  );
};
