import React, { Component } from 'react';
import { observer } from 'mobx-react';
import QuestionLoader from './QuestionLoader';
import TypeSelection from './TypeSelection';
import DataAnalysis from './DataAnalysis';
import OptionsSelection from './OptionsSelection';
import ShowOptionEditor from './ShowOptionEditor';
import {
  SettingsContainer,
  SettingsBlock,
  SettingsSwitcher,
} from '../../../styled/QuestionSettingsStyled';
import DataSaver from './DataSaver';
import ShareLink from './ShareLink';
import questionnaireStore from '../../../stores/questionnaire';


class Settings extends Component {
  state = {
    closed: false,
  }

  handleShowOptionEditor = (e) => {
    questionnaireStore.showEdit.set(e.target.checked);
  };

  handleTypeSelection = (e) => {
    questionnaireStore.questionType.set(+e.target.value);
  };

  handleSettingsSwitcher = () => {
    this.setState((state, props) => ({
      closed: !state.closed,
    }));
  }

  render() {
    const showEdit = questionnaireStore.showEdit.get();
    const questionType = questionnaireStore.questionType.get();

    let settingsBlock = null;
    if (!this.state.closed) {
      settingsBlock = (
        <SettingsBlock>
          <QuestionLoader />
          <TypeSelection
            selected={questionType}
            handleChange={this.handleTypeSelection}
          />
          <ShowOptionEditor
            selected={showEdit}
            handleChange={this.handleShowOptionEditor}
          />
          <OptionsSelection />
          <DataAnalysis />
          <DataSaver />
          <ShareLink />
        </SettingsBlock>
      );
    }

    return (
      <SettingsContainer>
        <SettingsSwitcher onClick={this.handleSettingsSwitcher} closed={this.state.closed} />
        {settingsBlock}
      </SettingsContainer>
    );
  }
}


export default observer(Settings);
