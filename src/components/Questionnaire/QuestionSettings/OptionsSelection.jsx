import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { ButtonS } from '../../../styled/SettingsStyled';
import questionnaireStore from '../../../stores/questionnaire';
import OptionInput from './OptionInput';
import localizedStrings from '../../../localizedStrings';

class OptionsSelection extends Component {
  state = {
    addingNewOption: false,
  }

  getBody() {
    const options = [];
    questionnaireStore.options.forEach((value, key) => {
      options.push(
        <div key={key}>
          <div>
            {`${key} ${value.price} ${value.code}`}
            <ButtonS type="button" id={`${key}ghbdtngbljh4815162342`} onClick={this.handleDelete}>{localizedStrings.delete}</ButtonS>
          </div>
          <div />

        </div>,
      );
    });
    return options;
  }

  handleDelete = (e) => {
    questionnaireStore.options.delete(e.target.id.replace('ghbdtngbljh4815162342', ''));
  }

  handleChange = () => {
    this.setState((state, props) => ({ addingNewOption: !state.addingNewOption }));
  }

  getAddingForm = () => {
    if (this.state.addingNewOption) {
      return (
        <OptionInput addOption={
          (name, price, code, e) => {
            e.preventDefault();
            questionnaireStore.options.set(name, { price: +price, code });
          }
        }
        />
      );
    }
    return null;
  }

  render() {
    return (
      <div>
        <ButtonS type="button" onClick={this.handleChange}>{!this.state.addingNewOption ? localizedStrings.addOption : localizedStrings.close }</ButtonS>
        {this.getBody()}
        {this.getAddingForm()}
      </div>
    );
  }
}

export default observer(OptionsSelection);
