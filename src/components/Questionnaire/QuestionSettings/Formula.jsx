import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import localizedStrings from '../../../localizedStrings';
import questionnaireStore from '../../../stores/questionnaire';
import { ButtonS } from '../../../styled/SettingsStyled';

class Formula extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
  };

  handleChange = (e) => {
    const { name } = this.props;

    const input = e.target.value;

    questionnaireStore.formulaFields.set(
      name, input,
    );
  }

  removeBtn = () => {
    const { name } = this.props;

    questionnaireStore.formulaFields.delete(name);
  }

  render() {
    const { name } = this.props;

    return (
      <div>
        {name}
        <input
          name={name}
          type="string"
          value={questionnaireStore.formulaFields.get(name)}
          onChange={this.handleChange}
        />
        {' '}
        {questionnaireStore.results.get(name)}
        <ButtonS type="button" onClick={this.removeBtn}>{localizedStrings.delete}</ButtonS>
      </div>
    );
  }
}

export default observer(Formula);
