import React, { Component } from 'react';
import { observer } from 'mobx-react';
import localizedStrings from '../../../localizedStrings';
import questionnaireStore from '../../../stores/questionnaire';
import Counter from './Counter';
import Formula from './Formula';
import { ButtonS } from '../../../styled/SettingsStyled';

class DataAnalysis extends Component {
  getBodyCounters = () => {
    const val = [];
    questionnaireStore.countFields.forEach((value, key) => {
      val.push(<Counter
        name={key}
        key={key}
      />);
    });
    return val;
  }

  getBodyFormalas = () => {
    const val = [];
    questionnaireStore.formulaFields.forEach((value, key) => {
      val.push(<Formula
        name={key}
        key={key}
      />);
    });
    return val;
  }

  handleClickC = () => {
    const name = prompt(localizedStrings.optionName, 'default');
    questionnaireStore.countFields.set(name, '');
  }

  handleClickF = () => {
    const name = prompt(localizedStrings.optionName, 'default');
    questionnaireStore.formulaFields.set(name, '');
  }

  render() {
    return (
      <div>
        <div>
          <ButtonS type="button" onClick={this.handleClickC}>{localizedStrings.addCounter}</ButtonS>
          {this.getBodyCounters()}
        </div>
        <div>
          <ButtonS type="button" onClick={this.handleClickF}>{localizedStrings.addFormula}</ButtonS>
          {this.getBodyFormalas()}
        </div>
      </div>
    );
  }
}

export default observer(DataAnalysis);
