import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { FormOption, InputS } from '../../../styled/SettingsStyled';
import localizedStrings from '../../../localizedStrings';

class OptionInput extends Component {
  static propTypes = {
    addOption: PropTypes.func.isRequired,
  }

  state = {
    name: 'name',
    price: 0,
    code: 'a',
  }

  handleChange = (e) => {
    const { id, value } = e.target;
    switch (id) {
      case 'option_name':
        this.setState({ name: value });
        break;
      case 'option_price':
        this.setState({ price: +value });
        break;
      case 'option_code':
        this.setState({ code: value });
        break;
      default:
        this.setState({
          name: 'name',
          price: 0,
          code: 'a',
        });
    }
  }

  render() {
    const { name, price, code } = this.state;
    return (
      <FormOption>
        {localizedStrings.optionName}
        <input value={name} id="option_name" onChange={this.handleChange} />
        {localizedStrings.optionPrice}
        <input value={price} id="option_price" onChange={this.handleChange} />
        {localizedStrings.optionCode}
        <input value={code} id="option_code" onChange={this.handleChange} />
        <InputS
          type="submit"
          value={localizedStrings.optionSubmit}
          onClick={this.props.addOption.bind(this, name, price, code)}
        />
      </FormOption>
    );
  }
}

export default OptionInput;
