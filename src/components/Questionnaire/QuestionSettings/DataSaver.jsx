import React from 'react';
import { toJS } from 'mobx';
import { Mutation } from 'react-apollo';
import { observer } from 'mobx-react';
import questionnaireStore from '../../../stores/questionnaire';
import userStore from '../../../stores/userStore';
import localizedStrings from '../../../localizedStrings';
import { ButtonS } from '../../../styled/SettingsStyled';
import { SET_QUESTIONNAIRE } from '../../../gql/questionnaire';


const DataSaver = (props) => {
  const {
    questions,
    options,
    individualOptions,
    questionType,
    countFields,
    formulaFields,
  } = questionnaireStore;
  if (userStore.sub.get()) {
    return (
      <Mutation mutation={SET_QUESTIONNAIRE}>
        {insertQuestionnaire => (
          <ButtonS
            type="button"
            onClick={(e) => {
              const name = prompt('enter name', '000');
              insertQuestionnaire({
                variables: {
                  objects: [
                    {
                      questions: JSON.stringify(toJS(questions)),
                      countFields: JSON.stringify(toJS(countFields)),
                      formulaFields: JSON.stringify(toJS(formulaFields)),
                      individualOptions: JSON.stringify(toJS(individualOptions)),
                      name,
                      options: JSON.stringify(toJS(options)),
                      qestionsType: questionType.get(),
                      user_id: userStore.sub,
                    },
                  ],
                },
              });
            }}
          >
            {localizedStrings.save}
          </ButtonS>
        )}
      </Mutation>
    );
  }
  return null;
};

export default observer(DataSaver);
