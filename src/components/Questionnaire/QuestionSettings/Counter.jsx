import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import localizedStrings from '../../../localizedStrings';
import questionnaireStore from '../../../stores/questionnaire';
import { ButtonS } from '../../../styled/SettingsStyled';

class Counter extends Component {
    static propTypes = {
      name: PropTypes.string.isRequired,
    };


    handleChange = (e) => {
      const { name } = this.props;
      const input = e.target.value;
      questionnaireStore.countFields.set(name, input);
    }

    removeBtn = () => {
      const { name } = this.props;
      questionnaireStore.countFields.delete(name);
    }

    render() {
      const { name } = this.props;

      return (
        <div>
          {name}
          <input
            name={name}
            type="string"
            value={questionnaireStore.countFields.get(name)}
            onChange={this.handleChange}
          />
          {' '}
          {questionnaireStore.results.get(name)}
          <ButtonS type="button" onClick={this.removeBtn}>{localizedStrings.delete}</ButtonS>
        </div>
      );
    }
}

export default observer(Counter);
