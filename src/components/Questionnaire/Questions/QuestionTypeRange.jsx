/* eslint-disable react/jsx-no-bind */

import React from 'react';
import PropTypes from 'prop-types';
// styled
import { SelectOption } from '../../../styled/QuestionStyled';

const QuestionTypeRange = (props) => {
  const { options, index, handleChange } = props;

  const body = [];
  let selected = -1;
  options.forEach((value) => {
    if (value.isSelected) {
      selected = value.name;
    }
    body.push(<option key={value.name}>{value.name}</option>);
  });
  if (selected === -1) {
    body.unshift(<option key={-1} />);
  }
  return (
    <SelectOption
      value={selected}
      onChange={handleChange.bind(this, index)}
    >
      {body}
    </SelectOption>
  );
};

QuestionTypeRange.propTypes = {
  index: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      isSelected: PropTypes.bool.isRequired,
    }).isRequired,
  ).isRequired,
};

export default QuestionTypeRange;
