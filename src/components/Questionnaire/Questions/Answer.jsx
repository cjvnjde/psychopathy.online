import React from 'react';
import PropTypes from 'prop-types';
// components
import QuestionTypeRadio from './QuestionTypeRadio';
import QuestionTypeRange from './QuestionTypeRange';

const Answer = (props) => {
  const {
    type, index, handleChange, options,
  } = props;

  switch (type) {
    case 1:
      return (
        <QuestionTypeRadio
          index={index}
          handleChange={handleChange}
          options={options}
        />
      );
    case 2:
      return (
        <QuestionTypeRange
          index={index}
          handleChange={handleChange}
          options={options}
        />
      );
    default:
      return null;
  }
};

Answer.propTypes = {
  index: PropTypes.number.isRequired,
  type: PropTypes.number,
  handleChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      isSelected: PropTypes.bool.isRequired,
    }).isRequired,
  ),
};

Answer.defaultProps = {
  type: 0,
};


export default Answer;
