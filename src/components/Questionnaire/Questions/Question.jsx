import React from 'react';
import PropTypes from 'prop-types';
// components
import Answer from './Answer';
// styled
import {
  QuestionContainer,
  QuestionIndex,
  QuestionText,
  QuestionBlock,
  AnswerBlock,
} from '../../../styled/QuestionStyled';

const Question = (props) => {
  const {
    index, text, type, handleChange, options, isChanged,
  } = props;

  return (
    <QuestionContainer isChanged={isChanged}>
      <QuestionIndex>{index + 1}</QuestionIndex>
      <QuestionBlock>
        <QuestionText>{text}</QuestionText>
        <AnswerBlock>
          <Answer
            type={type}
            index={index}
            handleChange={handleChange}
            options={options}
          />
        </AnswerBlock>
      </QuestionBlock>
    </QuestionContainer>
  );
};

Question.propTypes = {
  text: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  type: PropTypes.number,
  handleChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      isSelected: PropTypes.bool.isRequired,
    }).isRequired,
  ),
  isChanged: PropTypes.bool,
};

Question.defaultProps = {
  type: 0,
  isChanged: false,
  options: [{ name: '', isSelected: false }],
};

export default Question;
