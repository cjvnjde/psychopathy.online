import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
// store
import questionnaireStore from '../../../stores/questionnaire';
// helpers
import localizedStrings from '../../../localizedStrings';
// components
import { ButtonEdit } from '../../../styled/QuestionContainer';

class AnswerEdit extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
  }

  state = {
    areChanging: false,
  }

  changeOptions = () => {
    this.setState((state, props) => (
      { areChanging: !state.areChanging }));
  }

  changeValue = (name, key, e) => {
    const { code, price } = questionnaireStore
      .individualOptions
      .get(this.props.index)
      .get(key);

    switch (name) {
      case 'key':
        questionnaireStore
          .individualOptions
          .get(this.props.index).delete(key);
        questionnaireStore
          .individualOptions
          .get(this.props.index)
          .set(e.target.value, { code, price });
        break;
      case 'code':
        questionnaireStore
          .individualOptions
          .get(this.props.index)
          .set(key, { code: e.target.value, price });
        break;
      case 'price':
        questionnaireStore
          .individualOptions
          .get(this.props.index)
          .set(key, { code, price: +e.target.value });
        break;
      default:
    }
  }

  getBody = () => {
    if (this.state.areChanging) {
      const b = [];

      questionnaireStore.individualOptions.get(this.props.index)
        .forEach((value, key) => {
          b.push(
            <div key={key}>
              <input
                value={key}
                onChange={this.changeValue.bind(this, 'key', key)}
              />
              <input
                value={
              questionnaireStore
                .individualOptions
                .get(this.props.index)
                .get(key)
                .code
            }
                onChange={this.changeValue.bind(this, 'code', key)}
              />
              <input
                value={
              questionnaireStore
                .individualOptions
                .get(this.props.index)
                .get(key)
                .price
            }
                onChange={this.changeValue.bind(this, 'price', key)}
              />
            </div>,
          );
        });
      return b;
    }
    return null;
  }

  getAnswerBody = () => {
    if (questionnaireStore.showEdit.get()
      && (+questionnaireStore.questionType.get() === 1
      || +questionnaireStore.questionType.get() === 2)) {
      return (
        <React.Fragment>
          <ButtonEdit
            type="button"
            onClick={this.changeOptions}
          >
            {localizedStrings.edit}
          </ButtonEdit>
          {this.getBody()}
        </React.Fragment>
      );
    }
    return null;
  }

  render() {
    return this.getAnswerBody();
  }
}

export default observer(AnswerEdit);
