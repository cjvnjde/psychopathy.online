import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import questionnaireStore from '../../../stores/questionnaire';
// components
import Question from './Question';
import AnswerEdit from './AnswerEdit';

class QuestionsList extends Component {
    setValue = (index, value) => {
      questionnaireStore.selectedValues.set(+index, value);
    }

    handleChange = (index, e) => {
      questionnaireStore.selectedValues.set(
        +index,
        Object.assign(questionnaireStore
          .individualOptions
          .get(index)
          .get(e.target.value), { value: e.target.value }),
      );
    }

    getOptions = () => {
      const body = [];
      const changed = [];
      questionnaireStore.individualOptions.forEach((value1, key1) => {
        const opt = [];
        let isChanged = false;
        let value;
        for (const key in toJS(value1)) {
          if (Object.prototype.hasOwnProperty.call(toJS(value1), key)) {
            let isSelected = false;
            value = questionnaireStore.selectedValues.get(key1);
            if ((value !== undefined) && (value !== -1) && (value.value === key)) {
              isSelected = true;
              isChanged = true;
            }
            opt.push({
              name: key,
              isSelected,
            });
          }
        }
        if (value !== undefined) {
          changed.push(isChanged);
          body.push(opt);
        }
      });
      // if (!changed.includes(false)) { questionnaireStore.allChanged.set(changed.includes(false)); }
      return { body, changed };
    }

    render() {
      const type = +questionnaireStore.questionType.get();
      const options = this.getOptions();
      const questions = questionnaireStore.questions.map((value) => {
        const { index, question } = value;
        if (question) {
          return (
            <div key={index}>
              <Question
                text={question}
                index={index}
                type={type}
                handleChange={this.handleChange}
                options={options.body[index]}
                isChanged={options.changed[index]}
              />
              {questionnaireStore.options.size > 0 ? <AnswerEdit index={index} /> : null}
            </div>
          );
        }
        return null;
      });
      return (questions);
    }
}

export default observer(QuestionsList);
