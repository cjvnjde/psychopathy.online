/* eslint-disable react/jsx-no-bind */

import React from 'react';
import PropTypes from 'prop-types';
// styled
import {
  RadioBlock,
  Label,
  OptionBlock,
  LabelText,
} from '../../../styled/QuestionStyled';

const QuestionTypeRadio = (props) => {
  const { options, index, handleChange } = props;
  const body = [];
  options.forEach((value) => {
    body.push(
      <Label htmlFor={value.name + index} key={value.name}>
        <OptionBlock style={{ display: 'flex' }}>
          <RadioBlock>
            <input
              type="radio"
              id={value.name + index}
              value={value.name}
              checked={value.isSelected}
              onChange={handleChange.bind(this, index)}
            />
          </RadioBlock>
          <LabelText>
            {value.name}
          </LabelText>
        </OptionBlock>
      </Label>,
    );
  });
  return body;
};

QuestionTypeRadio.propTypes = {
  index: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      isSelected: PropTypes.bool.isRequired,
    }).isRequired,
  ).isRequired,
};

export default QuestionTypeRadio;
