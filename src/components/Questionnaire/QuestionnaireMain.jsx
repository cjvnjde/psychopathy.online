import React from 'react';
// components
import Settings from './QuestionSettings/Settings';
import QuestionsList from './Questions/QuestionsList';


const QuestionnaireMain = () => (
  <div>
    <Settings />
    <QuestionsList />
  </div>
);

export default QuestionnaireMain;
