import React from 'react';
import Settings from './Questionnaire/QuestionSettings/Settings';

export default () => (
  <div>
    <Settings />
  </div>
);
