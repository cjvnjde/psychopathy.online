import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { List } from 'react-virtualized';
import { observer } from 'mobx-react';
import questionnaireStore from '../stores/questionnaire';
import questionnairesStore from '../stores/questionnaires';
import { GET_QUESTIONNAIRE } from '../gql/questionnaire';
import { QuestionnaireList } from '../styled/Questionnaires';
import { objToStore } from '../helpers';

class SharedAnswersList extends Component {
      dataParser = (obj) => {
        questionnaireStore.questions.clear();
        const {
          questions, qestionsType, countFields, formulaFields, individualOptions, options,
        } = obj;
        questionnaireStore.selectedValues.clear();
        JSON.parse(questions).forEach((element) => {
          questionnaireStore.questions.push(element);
          questionnaireStore.selectedValues.set(+element.index, -1);
        });

        questionnaireStore.questionType.set(qestionsType);
        objToStore(JSON.parse(countFields), questionnaireStore.countFields);
        objToStore(JSON.parse(formulaFields), questionnaireStore.formulaFields);
        objToStore(JSON.parse(individualOptions), questionnaireStore.individualOptions);
        objToStore(JSON.parse(options), questionnaireStore.options);
      }


      rowRenderer = ({
        index,
        isScrolling,
        isVisible,
        key,
        parent,
        style,
      }) => (
        <ApolloConsumer key={key}>
          {(client) => {
            // JSON.parse(questionnairesStore.answers[index].answers).forEach((element) => {
            //   console.log(element);
            //   questionnaireStore.selectedValues.set(index, element.price);
            // });
            // console.log(questionnairesStore.answers);
            const ansId = questionnairesStore.answers[index].questions_id;
            return (
              <div
                key={key}
                style={style}
                onClick={async () => {
                  // JSON.parse(questionnairesStore.answers[index].answers).forEach((element) => {
                  //   console.log(element);
                  //   questionnaireStore.selectedValues.set(index, element.price);
                  // });
                  const idd = ansId.split('boobs')[0];
                  questionnaireStore.questionnaireId.set(ansId.split('boobs')[0]);
                  const { data } = await client.query({
                    query: GET_QUESTIONNAIRE,
                    variables: { id: idd },
                  });
                  this.dataParser(data.questionnaire[0]);

                  questionnaireStore.selectedValues.clear();
                  const obj2 = JSON.parse(questionnairesStore.answers[index].answers);
                  for (const key in obj2) {
                    if (obj2.hasOwnProperty(+key)) {
                      questionnaireStore.selectedValues.set((+key), obj2[key]);
                      // console.log(999999999, (+key) - 1, obj2[key]);
                    }
                  }
                }}
              >
                <QuestionnaireList>
                  {questionnairesStore.names[index]}
                  <br />
                  {`${index} ${questionnairesStore.answers[index].respondent_nickname}`}
                  {/* <br />
                  <input
                    type="checkbox"
                  /> */}
                </QuestionnaireList>

              </div>
            );
            // this.dataParser(data.questionnaire[0]);
          }
          }

        </ApolloConsumer>
      )

      render() {
        return (
          <List
            width={400}
            height={500}
            rowCount={questionnairesStore.answers.length}
            rowHeight={60}
            rowRenderer={this.rowRenderer}
          />
        );
      }
}

export default observer(SharedAnswersList);
