import React, { Component } from 'react';
import { Query, ApolloConsumer } from 'react-apollo';
import { List } from 'react-virtualized';
import userStore from '../stores/userStore';
import questionnaireStore from '../stores/questionnaire';
import { GET_QUESTIONNAIRES, GET_QUESTIONNAIRE } from '../gql/questionnaire';
import Loading from './common/Loading';
import { QuestionnaireList, QuestionnairesDBContainer } from '../styled/Questionnaires';
import SharedAnswers from './SharedAnswers';
import { objToStore } from '../helpers';

class Questionnaires extends Component {
      dataParser = (obj) => {
        questionnaireStore.questions.clear();
        const {
          questions, qestionsType, countFields, formulaFields, individualOptions, options,
        } = obj;
        questionnaireStore.selectedValues.clear();
        JSON.parse(questions).forEach((element) => {
          questionnaireStore.questions.push(element);
          questionnaireStore.selectedValues.set(+element.index, -1);
        });
        questionnaireStore.questionType.set(qestionsType);
        objToStore(JSON.parse(countFields), questionnaireStore.countFields);
        objToStore(JSON.parse(formulaFields), questionnaireStore.formulaFields);
        objToStore(JSON.parse(options), questionnaireStore.options);
        questionnaireStore.individualOptions.clear();
        const iops = JSON.parse(individualOptions);
        for (const key in iops) {
          if (Object.prototype.hasOwnProperty.call(iops, key)) {
            if (Number.parseInt(key, 10) || (Number.parseInt(key, 10) === 0)) {
              const map = new Map();
              for (const key2 in iops[key]) {
                if (Object.prototype.hasOwnProperty.call(iops[key], key2)) {
                  map.set(key2, iops[key][key2]);
                }
              }
              questionnaireStore.individualOptions.set(+key, map);
            } else {
              const map = new Map();
              for (const key2 in iops[key]) {
                if (Object.prototype.hasOwnProperty.call(iops[key], key2)) {
                  map.set(key2, iops[key][key2]);
                }
              }
              questionnaireStore.individualOptions.set(key, map);
            }
          }
        }
      }

      render() {
        return (
          <QuestionnairesDBContainer>
            <ApolloConsumer>
              {client => (
                <Query
                  query={GET_QUESTIONNAIRES}
                  variables={{ user_id: userStore.sub }}
                  fetchPolicy="network-only"
                >
                  {({ loading, error, data }) => {
                    if (loading) return <Loading />;
                    if (error) return <div>Error :(</div>;
                    const list = data.questionnaire.map(
                      value => (
                        { id: value.id, name: value.name }
                      ),
                    );
                    function rowRenderer({
                      index,
                      isScrolling,
                      isVisible,
                      key,
                      parent,
                      style,
                    }) {
                      return (
                        <div
                          key={key}
                          style={style}
                          onClick={async () => {
                            questionnaireStore.questionnaireId.set(list[index].id);
                            const { data } = await client.query({
                              query: GET_QUESTIONNAIRE,
                              variables: { id: list[index].id },
                            });
                            this.dataParser(data.questionnaire[0]);
                          }}
                        >
                          <QuestionnaireList>
                            {`${index} ${list[index].name}`}
                          </QuestionnaireList>

                        </div>
                      );
                    }
                    return (
                      <List
                        width={300}
                        height={500}
                        rowCount={list.length}
                        rowHeight={30}
                        rowRenderer={rowRenderer.bind(this)}
                      />
                    );
                  }}
                </Query>
              )}
            </ApolloConsumer>
            <SharedAnswers />
          </QuestionnairesDBContainer>
        );
      }
}

export default Questionnaires;
