import React, { Component } from 'react';
import { Query, ApolloConsumer } from 'react-apollo';
import userStore from '../stores/userStore';
import questionnaireStore from '../stores/questionnaire';
import { GET_SHARED_LINKS, GET_SHARED_ANSWERS, GET_QUESTIONNAIRE_NAME } from '../gql/questionnaire';
import Loading from './common/Loading';
import SharedAnswersList from './SharedAnswersList';
import questionnairesStore from '../stores/questionnaires';
import { objToStore } from '../helpers';

class SharedAnswers extends Component {
      dataParser = (obj) => {
        questionnaireStore.questions.clear();
        const {
          questions, qestionsType, countFields, formulaFields, individualOptions, options,
        } = obj;
        questionnaireStore.selectedValues.clear();
        JSON.parse(questions).forEach((element) => {
          questionnaireStore.questions.push(element);
          questionnaireStore.selectedValues.set(+element.index, -1);
        });
        questionnaireStore.questionType.set(qestionsType);
        objToStore(JSON.parse(countFields), questionnaireStore.countFields);
        objToStore(JSON.parse(formulaFields), questionnaireStore.formulaFields);
        objToStore(JSON.parse(individualOptions), questionnaireStore.individualOptions);
        objToStore(JSON.parse(options), questionnaireStore.options);
      }

      getAnswers = async (list, client) => {
        const answers = [];
        const arr = [];
        for (let i = 0; i < list.length; i += 1) {
          answers.push(client.query({
            query: GET_SHARED_ANSWERS,
            variables: { questions_id: list[i] },
          }));
        }
        Promise.all(answers).then((value) => {
          value.forEach((v) => {
            const { data } = v;
            if (data.answers.length) {
              data.answers.forEach((value0) => {
                arr.push(value0);
              });
            }
          });

          arr.sort((a, b) => {
            if (a.id < b.id) return -1;
            if (a.id > b.id) return 1;
            return 0;
          });

          const names = [];
          for (let j = 0; j < arr.length; j += 1) {
            const idd = arr[j].questions_id.split('boobs')[0];
            names.push(client.query({
              query: GET_QUESTIONNAIRE_NAME,
              variables: { id: idd },
            }));
          }
          Promise.all(names).then((va) => {
            questionnairesStore.names.clear();
            questionnairesStore.answers.clear();

            va.forEach((value1) => {
              questionnairesStore.names.push(value1.data.questionnaire[0].name);
            });
            arr.forEach((value2) => {
              questionnairesStore.answers.push(value2);
            });
          });
        });
      }


      render() {
        return (
          <React.Fragment>
            <ApolloConsumer>
              {client => (
                <Query
                  query={GET_SHARED_LINKS}
                  variables={{ user_id: userStore.sub }}
                >
                  {({ loading, error, data }) => {
                    if (loading) return <Loading />;
                    if (error) return <div>Error :(</div>;

                    const list = data.shared_links.map(value => value.link);
                    this.getAnswers(list, client);
                    return (
                      <SharedAnswersList />
                    );
                  }}
                </Query>
              )}

            </ApolloConsumer>

          </React.Fragment>
        );
      }
}

export default SharedAnswers;
