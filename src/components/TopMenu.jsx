/* eslint-disable react/jsx-no-bind */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LanguagePicker from './LanguagePicker';
import {
  Header, UlHeader, LiHeader, LiHeader2, AHeader,
} from '../styled/Header';
import localizedStrings from '../localizedStrings';

class TopMenu extends Component {
  static propTypes = {
    auth: PropTypes.shape({
      renewSession: PropTypes.func,
      login: PropTypes.func,
      logout: PropTypes.func,
      isAuthenticated: PropTypes.func,
    }).isRequired,
    history: PropTypes.shape({
      replace: PropTypes.func,
    }).isRequired,
  }

  componentDidMount() {
    const { renewSession } = this.props.auth;

    if (localStorage.getItem('isLoggedIn') === 'true') {
      renewSession();
    }
  }

  login = () => {
    this.props.auth.login();
  }

  logout = () => {
    this.props.auth.logout();
  }

  getMenu = () => {
    const { isAuthenticated } = this.props.auth;
    return (
      <UlHeader>
        <LiHeader exact to="">
          {localizedStrings.mainPage}
        </LiHeader>
        <LiHeader exact to="/questionnaire">
          {localizedStrings.questionnaire}
        </LiHeader>
        <LiHeader exact to="/spreadsheet">
          Spreadsheet
        </LiHeader>
        {!isAuthenticated() ? (
          <LiHeader2 onClick={this.login}>
            {localizedStrings.signIn}
          </LiHeader2>) : (
            <React.Fragment>
              <LiHeader exact to="/myquestionnaires">
                {localizedStrings.myQuestionnaires}
              </LiHeader>
              <LiHeader2 onClick={this.logout}>
                {localizedStrings.signOut}
              </LiHeader2>
            </React.Fragment>
        )}
        <LanguagePicker />
        <AHeader href="https://gitlab.com/cjvnjde/psychopathy.online/blob/master/README.md">
          {localizedStrings.doc}
        </AHeader>
      </UlHeader>
    );
  };

  render() {
    return (
      <Header>
        {this.getMenu()}
      </Header>
    );
  }
}

export default TopMenu;
