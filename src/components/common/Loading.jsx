import React from 'react';
import PropTypes from 'prop-types';
import Loader from '../../styled/common/Loader';

const Loading = props => (<Loader {...props} />);
Loading.propTypes = {
  size: PropTypes.number,
};
Loading.defaultProps = {
  size: 60,
};
export default Loading;
