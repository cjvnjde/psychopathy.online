import React from 'react';
// styled
import { NeedAuthBtn, NeedAuthContainer } from '../../styled/common/Loader';
// context
import { AuthContext } from '../../routes';


const NeedAuth = props => (
  <NeedAuthContainer>
    <AuthContext.Consumer>
      {auth => (
        <NeedAuthBtn onClick={auth.login}>
          Нужна авторизация
        </NeedAuthBtn>
      )}
    </AuthContext.Consumer>
  </NeedAuthContainer>
);

export default NeedAuth;
