import { gql } from 'apollo-boost';

export const GET_QUESTIONNAIRES = gql`
query 
  questionnaire($user_id: String!) {
  questionnaire(where: {user_id :{_eq: $user_id}}) {
    id
    name
  }
}
`;

export const GET_QUESTIONNAIRE = gql`
query 
  questionnaire($id: Int!) {
  questionnaire(where: {id :{_eq: $id}}) {
    countFields
    formulaFields
    id
    individualOptions
    name
    options
    qestionsType
    questions
    user_id
  }
}
`;

export const SET_QUESTIONNAIRE = gql`
mutation insert_questionnaire($objects: [questionnaire_insert_input!]!){
  insert_questionnaire(objects:$objects) {
    affected_rows
    returning {
      id
    }
}
}
`;


export const SET_SHARED_LINK = gql`
mutation insert_shared_links($objects: [shared_links_insert_input!]!){
  insert_shared_links(objects:$objects) {
    affected_rows
    returning {
      id
    }
}
}
`;

export const GET_QUESTIONNAIRE_ID = gql`
query 
shared_links($link: String!) {
  shared_links(where: {link :{_eq: $link}}) {
    questions_id
  }
}
`;

export const SET_ANSAWERS = gql`
mutation insert_answers($objects: [answers_insert_input!]!){
  insert_answers(objects:$objects) {
    affected_rows
    returning {
      id
    }
}
}
`;

export const GET_SHARED_LINKS = gql`
query 
shared_links($user_id: String!) {
  shared_links(where: {user_id :{_eq: $user_id}}) {
    link
    questions_id
  }
}
`;

export const GET_SHARED_ANSWERS = gql`
query 
answers($questions_id: String!) {
  answers(
    order_by: {time: asc}
    where: {questions_id :{_eq: $questions_id}}) {
    respondent_nickname
    questions_id
    id
    answers
    time
  }
}
`;

export const GET_SHARED_ANSWER = gql`
query 
answers($id: String!) {
  answers(where: {id :{_eq: $id}}) {
    answers
  }
}
`;

export const GET_QUESTIONNAIRE_NAME = gql`
query 
  questionnaire($id: Int!) {
  questionnaire(where: {id :{_eq: $id}}) {
    name
  }
}
`;
