import styled from 'styled-components';

export const QuestionnaireList = styled.div`
    background-color: #e6e6e6;
    color: #4d4d4d;
    display: flex;
    flex-direction: row;
    border-radius: 4px;
    height: 60px;
    cursor: pointer;
    :hover {
        background-color: #d4d4d4;
        color: #666666;
    }
`;

export const QuestionnairesDBContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    
`;
