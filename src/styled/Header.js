import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export const Header = styled.header`
    margin: 0px;
    position: sticky;
    top: 0;
    width: 100%;
    background-color: #e6e6e6;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-start;
    align-items: center;
    box-shadow: 0px -20px 10px 20px grey;
    z-index: 9999;
`;

export const UlHeader = styled.ul`
    list-style-type: none;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-start;
    align-items: center;
    margin: 0;
`;

export const LiHeader2 = styled.li`
    color: #4d4d4d;
    align-items: center;
    margin: 4px 2px;
    padding: 6px 8px;
    border-radius: 4px;
    cursor: pointer;
    :hover {
        background-color: #d4d4d4;
        color: #666666;
    }
`;

export const LiHeader = styled(NavLink)`
    color: #4d4d4d;
    align-items: center;
    margin: 4px 2px;
    padding: 6px 8px;
    border-radius: 4px;
    cursor: pointer;
    text-decoration: none;
    :hover {
        background-color: #d4d4d4;
        color: #666666;
    }
    &.active {
        background-color: #d4d4d4;
        color: #666666;
      }
`;

export const AHeader = styled.a`
    color: #4d4d4d;
    align-items: center;
    margin: 4px 2px;
    padding: 6px 8px;
    border-radius: 4px;
    cursor: pointer;
    text-decoration: none;
    :hover {
        background-color: #d4d4d4;
        color: #666666;
    }
    &.active {
        background-color: #d4d4d4;
        color: #666666;
      }
`;
