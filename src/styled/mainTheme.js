export default {
  darkPrimary: '#455A64',
  lightPrimary: '#CFD8DC',
  primaryColor: '#607D8B',
  text: '#FFFFFF',
  accentColor: '#00BCD4',
  primaryText: '#212121',
  secondaryText: '#757575',
  dividerColor: '#BDBDBD',
  lightDividerColor: '#f2f2f2',
  backgroundColor: '#FFFFFF',


  // primary: '#EAE7DC',
  // primaryVariant: '#D8C3A5',
  // secondary: '#8E8D8A',
  // secondaryVariant: '#E98074',

  // background: '#E85A4F',
  // surface: '#E85A4F',
  // error: '#E85A4F',

  // onPrimary: '#E85A4F',
  // onSecondary: '#E85A4F',
  // onBackground: '#E85A4F',
  // onSurface: '#E85A4F',
  // onError: '#E85A4F',
};
