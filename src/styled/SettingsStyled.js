import styled from 'styled-components';

export const SettingsContainer = styled.div`
    padding-top: 5px;
    border-left: 2px solid grey;
    border-bottom: 2px solid grey;
    background-color: beige;
    position: fixed;
    right: 0;
    top: 48px;
    height: 100%;
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    overflow: scroll;
`;

export const InputLoad = styled.input`
  display: none;
`;

export const LabelLoad = styled.label`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    cursor: pointer;
    list-style-type: none;
    :hover {
            border: 1px solid red;
            padding: 5px 15px;
        }
`;

export const FormOption = styled.form`
display: flex;
flex-direction: column;
`;

export const ButtonS = styled.button`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    background-color: Transparent;
    cursor: pointer;
    :hover {
            border: 1px solid red;
            padding: 5px 15px;
        }
`;

export const ButtonSend = styled.button`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 50%;
    height: 50px;
    width: 50px;
    display: flex;
    justify-content: center;
    border: 1px solid grey;
    background-color: Transparent;
    position: fixed;
    right: 0;
    bottom: 0;
    cursor: pointer;
    :hover {
            border: 1px solid red;
            padding: 5px 15px;
        }
`;


export const InputS = styled.input`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    background-color: Transparent;
    cursor: pointer;
    :hover {
            border: 1px solid red;
            padding: 5px 15px;
        }
`;

export const SelectOption = styled.select`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    background-color: Transparent;
    cursor: pointer;
`;
