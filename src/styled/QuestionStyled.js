import styled from 'styled-components';
/*
darkPrimary: '#455A64',
  lightPrimary: '#CFD8DC',
  primaryColor: '#607D8B',
  text: '#FFFFFF',
  accentColor: '#00BCD4',
  primaryText: '#212121',
  secondaryText: '#757575',
  dividerColor: '#BDBDBD',
  backgroundColor: '#FFFFFF',
*/

export const QuestionContainer = styled.div`
   
    border: 1px solid ${props => props.theme.dividerColor};
    border-radius: 5px;
    max-width: 650px;
    display: flex;
    flex-direction: row;
    margin: 5px;
    @keyframes selecting {
        from  {
            background-color: ${props => props.theme.backgroundColor};;
        }
        to  {
            background-color: ${props => props.theme.lightPrimary};;
        }
    }
    animation: 0.5s ${props => (props.isChanged
    ? 'selecting'
    : '')};
    animation-fill-mode: forwards;
}
`;

// Question block
export const QuestionBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: left;
    margin: 5px;
`;

export const QuestionIndex = styled.div`
    background-color: ${props => props.theme.primaryColor};
    color: ${props => props.theme.text};
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 32px;
    padding: 5px;
    margin-right: 15px; 
`;

export const QuestionText = styled.div`
    max-width: 650px;
`;

// Answer block

export const AnswerBlock = styled.div`
    max-width: 650px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin: 5px;
    padding-top: 5px;
`;

export const RadioBlock = styled.div`
    background-color: ${props => props.theme.primaryColor};
    border-bottom-left-radius: 4px;
    border-top-left-radius: 4px;
    padding: 5px;
`;

export const Label = styled.label`
    cursor: pointer;
`;

export const LabelText = styled.div`
    margin-left: 10px;
    margin-right: 10px;
`;

export const OptionBlock = styled.div`
    border: 1px solid ${props => props.theme.dividerColor};
    border-radius: 5px;
    display: flex;
    align-items: center;
    margin: 5px 15px;
`;

export const SelectOption = styled.select`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 5px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: Transparent;
    cursor: pointer;
`;

export const ButtonEdit = styled.button`
    align-items: center;
    margin: 0px 5px;
    padding: 0px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    background-color: Transparent;
    cursor: pointer;
    :hover {
            border: 1px solid red;
        }
`;
