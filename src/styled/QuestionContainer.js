import styled from 'styled-components';

export const QuestionContainer = styled.div`
    background-color: antiquewhite;
    max-width: 650px;
    display: flex;
    flex-direction: row;
    align-items: center;
    margin: 5px;
`;

export const QuestionIndex = styled.div`
    max-width: 50px;
    border: 2px solid yellowgreen;
    border-radius: 3px;
    padding: 5px;
    margin-right: 15px; 
`;

export const QuestionText = styled.div`
    max-width: 650px;
`;
export const ButtonEdit = styled.button`
    
    align-items: center;
    margin: 0px 5px;
    padding: 0px 15px;
    border-radius: 4px;
    border: 1px solid grey;
    background-color: Transparent;
    cursor: pointer;
    :hover {
            border: 1px solid red;
        }
`;
