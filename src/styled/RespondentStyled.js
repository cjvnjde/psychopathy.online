import styled from 'styled-components';

export const RespondentContainer = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
`;
// pointer-events: ${props => (!props.disabled ? 'none' : 'auto')};
export const ButtonSend = styled.button`
    
    max-width: 650px;
    align-items: center;
    margin: 5px;
    margin-bottom: 40px;
    padding: 15px;
    border-radius: 4px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: ${props => props.theme.backgroundColor};
    cursor: pointer;
    list-style-type: none;
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
    :active {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.primaryColor};
            color: ${props => props.theme.text};
        }
`;
