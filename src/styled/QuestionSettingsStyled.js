import styled from 'styled-components';

/*
darkPrimary: '#455A64',
  lightPrimary: '#CFD8DC',
  primaryColor: '#607D8B',
  text: '#FFFFFF',
  accentColor: '#00BCD4',
  primaryText: '#212121',
  secondaryText: '#757575',
  dividerColor: '#BDBDBD',
  lightDividerColor: '#e6e6e6',
  backgroundColor: '#FFFFFF',
*/

export const SettingsContainer = styled.div`
    position: fixed;
    right: 0;
    top: 0;
    display: flex;
    flex-direction: row;
    height: 100%;
`;

export const SettingsBlock = styled.div`
    padding-top: 5px;
    background-color: ${props => props.theme.lightDividerColor};
    border-left: 1px solid ${props => props.theme.dividerColor};
    display: flex;
    flex-direction: column;
    flex-wrap: nowrap;
    overflow: scroll;
`;

export const SettingsSwitcher = styled.div`
    background-color: ${props => props.theme.lightDividerColor};
    border-left: 1px solid ${props => props.theme.dividerColor};
    height: 100%;
    display: flex;
    width: 15px;
    justify-content: center;
    align-items: center;
    ::after {
        content: "";
        height: 7px;
        width: 7px;
        border: solid ${props => props.theme.primaryColor};
        border-width: 0 3px 3px 0;
        display: block;
        transform: ${props => (!props.closed
    ? 'rotate(-45deg);'
    : 'rotate(135deg);')};
        
        
    }
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
    :active {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.primaryColor};
            color: ${props => props.theme.text};
            ::after {
                content: "";
                height: 7px;
                width: 7px;
                border: solid ${props => props.theme.lightPrimary};
                border-width: 0 3px 3px 0;
                display: block;
                transform: ${props => (!props.closed
    ? 'rotate(-45deg);'
    : 'rotate(135deg);')};
            }
        }
`;

export const InputLoad = styled.input`
  display: none;
`;

export const LabelLoad = styled.label`
    align-items: center;
    margin: 5px;
    margin-top: 40px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: ${props => props.theme.backgroundColor};
    cursor: pointer;
    list-style-type: none;
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
    :active {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.primaryColor};
            color: ${props => props.theme.text};
        }
`;

export const SelectOption = styled.select`
    align-items: center;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    min-height: 29px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: ${props => props.theme.backgroundColor};
    cursor: pointer;
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
`;

export const LabelShowEditEdit = styled.label`
    cursor: pointer;
    margin: 5px;
    padding: 5px 15px;
    border-radius: 4px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: ${props => props.theme.backgroundColor};
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
    :active {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.primaryColor};
            color: ${props => props.theme.text};
        }
`;
