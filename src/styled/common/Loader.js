import styled from 'styled-components';

export default styled.div`
  border: ${props => (`${props.size / 4}px`)} solid #f3f3f3; /* Light grey */
  border-top: ${props => (`${props.size / 4}px`)} solid #3498db; /* Blue */
  border-radius: 50%;
  width: ${props => (`${props.size}px`)};
  height: ${props => (`${props.size}px`)};
  animation: spin 2s linear infinite;
  @keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
`;

export const NeedAuthBtn = styled.button`
    align-items: center;
    margin: 5px;
    margin-top: 40px;
    padding: 40px;
    border-radius: 4px;
    border: 1px solid ${props => props.theme.dividerColor};
    background-color: ${props => props.theme.backgroundColor};
    cursor: pointer;
    list-style-type: none;
    :hover {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.lightPrimary};
        }
    :active {
            border: 1px solid ${props => props.theme.accentColor};
            background-color: ${props => props.theme.primaryColor};
            color: ${props => props.theme.text};
        }
`;

export const NeedAuthContainer = styled.div`
    background-color: rgba(0, 0, 0, 0.75);
    position: fixed;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 50px;
    top: 20px;
    left: 0px;
    height: 100%;
    width: 100%;
`;
