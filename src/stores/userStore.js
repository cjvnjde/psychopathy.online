import { observable } from 'mobx';

class UserStore {
  sub = observable.box('');

  nickname = observable.box('');
}

const userStore = new UserStore();

export default userStore;
