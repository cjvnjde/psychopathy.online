import {
  observable, reaction,
} from 'mobx';
import { Parser } from 'hot-formula-parser';
import cyrillicToTranslit from 'cyrillic-to-translit-js';
import numWords from 'num-words';

const parser = new Parser();

export class QuestionnaireStore {
  // questions
  questions = observable([]); // { index: 0, question: '' }

  // answers
  answers = observable([]);

  options = observable(new Map());
  // questionStandardType = observable.box(0);

  individualOptions = observable(new Map());

  // settings
  allquestionsAreSame = observable.box(false);

  questionType = observable.box(0);

  selectedValues = observable(new Map()); // id, 123

  // data analysis
  countFields = observable(new Map());

  formulaFields = observable(new Map());

  results = observable(new Map()); // index, 12

  showEdit = observable.box(false);

  questionnaireId = observable.box(0);

  hideSettings = observable.box(false);

  allChanged = observable.box(false);
}


const questionnaireStore = new QuestionnaireStore();

function parseValue(value) {
  if (value === undefined) return [];
  const v = value.split(',');
  return v;
}


function counter(input, name) {
  // const value = questionnaireStore.countFields.get(this.props.name);
  const vals = parseValue(input);
  let res = 0;
  const isWord = /[a-zA-z]+/;
  vals.forEach((val0) => {
    let numb = 1;
    let code = '';
    if (val0 && val0.match(isWord)) {
      numb = parseInt(val0.substring(0, val0.search(isWord)), 10) - 1;
      code = val0.substring(val0.search(isWord), val0.length).trim();
    } else {
      numb = parseInt(val0, 10) - 1;
      code = '';
    }
    if (questionnaireStore.selectedValues.has(numb)) {
      if (code === '') {
        res += questionnaireStore.selectedValues.get(numb).price;
      } else if (code === questionnaireStore.selectedValues.get(numb).code) {
        res += questionnaireStore.selectedValues.get(numb).price;
      }
    }
  });
  questionnaireStore.results.set(
    name, res,
  );
  return res;
}
/*
getOptions = () => {
      const body = [];
      const changed = [];
      questionnaireStore.individualOptions.forEach((value1, key1) => {
        const opt = [];
        let isChanged = false;
        let value;
        for (const key in toJS(value1)) {
          if (Object.prototype.hasOwnProperty.call(toJS(value1), key)) {
            let isSelected = false;
            value = questionnaireStore.selectedValues.get(key1);
            if ((value !== undefined) && (value !== -1) && (value.value === key)) {
              isSelected = true;
              isChanged = true;
            }
            opt.push({
              name: key,
              isSelected,
            });
          }
        }
        if (value !== undefined) {
          changed.push(isChanged);
          body.push(opt);
        }
      });
      //if (!changed.includes(false)) { questionnaireStore.allChanged.set(changed.includes(false)); }
      return { body, changed };
    }

*/
// reaction(
//   () => {
//     const selected = [];
//     questionnaireStore.selectedValues.forEach((value, key) => {
//       selected.push(value !== -1);
//     });
//     return ({
//       selected,
//       options: questionnaireStore.individualOptions,
//     });
//   },
//   (data) => {
//     if (!data.selected.includes(false)) {
//       questionnaireStore.allChanged
//         .set(data.selected.includes(false));
//     }
//     console.log(data.selected.includes(false));
//   },
// );
// если удаляем options нужно перестать учитывать
// выделенные и удаленные но оставить общее количество
reaction(
  () => ({
    optionSize: questionnaireStore.options.size,
    questionsSize: questionnaireStore.questions.length,
  }),
  (data) => {
    questionnaireStore.selectedValues.forEach((value, key) => {
      questionnaireStore.selectedValues.set(+key, -1);
      questionnaireStore.individualOptions.set(
        +key,
        questionnaireStore.options.toJS(),
      );
    });
  },
);

function valueTransformer(string) {
  // console.log(string);

  const isSpace = / /g;
  const isRus = /[а-яА-ЯЁё]/;
  const digit = /\d/;
  let transformed = string;

  if (digit.test(transformed)) {
    transformed = transformed.split('').map((value) => {
      if (digit.test(value)) { return numWords(value); }
      return value;
    }).join('');
  }
  if (isSpace.test(transformed)) {
    transformed = transformed.replace(isSpace, '_');
  }
  if (isRus.test(transformed)) {
    transformed = cyrillicToTranslit().transform(transformed);
  }

  return transformed;
}

function replaceAll(input) {
  const variableReg = /(.*\$\{+)(.*)(.*\}+)/g;
  let transformed = input;
  const ar1 = variableReg.exec(input);
  if (ar1) {
    transformed = input.replace(
      (ar1[1].slice(ar1[1].length - 2, ar1[1].length)
      + ar1[2]
      + ar1[3]),
      valueTransformer(ar1[2]),
    );
    transformed = replaceAll(transformed);
  }
  return transformed;
}

reaction(
  () => ({
    formula: Array.from(questionnaireStore.formulaFields.entries()),
    count: Array.from(questionnaireStore.countFields.entries()),
    selected: Array.from(questionnaireStore.selectedValues.entries()),
  }),
  (data) => {
    // console.log(data.selected);
    data.count.forEach((value) => {
      counter(value[1], value[0]);
    });

    data.formula.forEach((value) => {
      questionnaireStore.results.forEach((val, key) => {
        parser.setVariable(valueTransformer(key), val);
      });
      let input = value[1];

      const variableReg = /(.*\$\{+)(.*)(.*\}+)/g;
      if (variableReg.test(input)) {
        input = replaceAll(input);
      }
      const calc = parser.parse(input);

      if (!calc.error) {
        // parser.setVariable(value[0], calc.result);
        questionnaireStore.results.set(
          value[0], calc.result,
        );
      } else {
        // parser.setVariable(value[0], 0);
        questionnaireStore.results.set(
          value[0], 0,
        );
      }
    });
  },
);

export default questionnaireStore;
