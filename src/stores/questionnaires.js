import { observable } from 'mobx';

class Questionnaires {
  sub = observable([]);

  answers = observable([]);

  names = observable([]);
}

export default new Questionnaires();
