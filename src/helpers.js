export const objToStore = (obj, store) => {
  store.clear();
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      if (Number.parseInt(key, 10)) {
        store.set(+key, obj[key]);
      } else {
        store.set(key, obj[key]);
      }
    }
  }
};

export const dataParser = (obj, store) => {
  store.questions.clear();
  const {
    questions, qestionsType, individualOptions, options,
  } = obj;
  store.selectedValues.clear();
  JSON.parse(questions).forEach((element) => {
    store.questions.push(element);
    store.selectedValues.set(+element.index, -1);
  });
  store.questionType.set(qestionsType);
  // objToStore(JSON.parse(countFields), questionnaireStore.countFields);
  // objToStore(JSON.parse(formulaFields), questionnaireStore.formulaFields);

  store.individualOptions.clear();
  const iops = JSON.parse(individualOptions);
  for (const key in iops) {
    if (Object.prototype.hasOwnProperty.call(iops, key)) {
      if (Number.parseInt(key, 10) || (Number.parseInt(key, 10) === 0)) {
        const map = new Map();
        for (const key2 in iops[key]) {
          if (Object.prototype.hasOwnProperty.call(iops[key], key2)) {
            map.set(key2, iops[key][key2]);
          }
        }
        store.individualOptions.set(+key, map);
      } else {
        const map = new Map();
        for (const key2 in iops[key]) {
          if (Object.prototype.hasOwnProperty.call(iops[key], key2)) {
            map.set(key2, iops[key][key2]);
          }
        }
        store.individualOptions.set(key, map);
      }
    }
  }
  objToStore(JSON.parse(options), store.options);
};


export function columnToLetter(column0) {
  let temp;
  let letter = '';
  let column = column0;
  while (column > 0) {
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}

export function letterToColumn(letter) {
  let column = 0;
  const { length } = letter;
  for (let i = 0; i < length; i += 1) {
    column += (letter.charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
  }
  return column;
}

export function coordToString({ column, row }) {
  return `${columnToLetter(column + 1)}${row + 1}`;
}
