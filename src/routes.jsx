import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { ThemeProvider } from 'styled-components';
import TopMenu from './components/TopMenu';
import QuestionnaireMain from './components/Questionnaire/QuestionnaireMain';

import Callback from './components/Callback';
import Auth from './Auth/Auth';
import history from './history';
import Questionnaires from './components/Questionnaires';
import ForTesting from './components/ForTesting';
import Main from './components/Main';
import theme from './styled/mainTheme';
import Spreadsheet from './components/Spreadsheet/Spreadsheet';
import Respondent from './components/Questionnaire/Respondent/Respondent';

const client = new ApolloClient({ uri: 'https://psychopathy.herokuapp.com/v1alpha1/graphql' });
const auth = new Auth();

const handleAuthentication = ({ location }) => {
  if (/access_token|id_token|error/.test(location.hash)) {
    auth.handleAuthentication();
  }
};

export const AuthContext = React.createContext({ auth });

export const makeMainRoutes = () => (
  <ApolloProvider client={client}>
    <Router history={history}>
      <AuthContext.Provider value={auth}>
        <ThemeProvider theme={theme}>
          <div>
            <Route path="/" render={props => <TopMenu auth={auth} {...props} />} />
            <Switch>
              <Route path="/" exact render={props => <Main auth={auth} {...props} />} />
              <Route path="/questionnaire" exact component={QuestionnaireMain} />
              <Route path="/respondent:id" exact component={Respondent} />
              <Route path="/testing" exact render={props => <ForTesting {...props} />} />
              <Route path="/myquestionnaires" exact render={props => <Questionnaires auth={auth} {...props} />} />
              <Route path="/spreadsheet" exact render={props => <Spreadsheet {...props} />} />
              <Route
                path="/callback"
                render={(props) => {
                  handleAuthentication(props);
                  return <Callback {...props} />;
                }}
              />
            </Switch>
          </div>
        </ThemeProvider>
      </AuthContext.Provider>
    </Router>
  </ApolloProvider>
);
